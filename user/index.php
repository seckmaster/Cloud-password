<?php
	$_SESSION["user"] = test;
	var_dump($_SESSION);

	/* If user isn't loged in, go to home page */
	if(session_id() == '' || !isset($_SESSION)) {
		// header("Location: ../");
		// exit();
	}
		
	include_once("../database.php");
	function imageFileName($name) {
		$file = "../images/apps/" . $name . ".png";
		if(file_exists($file)) return $file;
		else return null;
	}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/inputs.css" />
<link rel="stylesheet" type="text/css" href="../css/colorbox.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../js/jquery.colorbox-min.js"></script>

<script>
	$(document).ready(function(){
		// lightbox
		$(".inline").colorbox({inline:true});
		// showing up user details
		$("#header .btn").click(function(event) {
			var usr = $("#user");
			if(usr.is(':hidden'))
				usr.css("display", "block");
			else
				usr.css("display", "none");
			event.stopPropagation();
		});
		$("html").click(function() {
			var usr = $("#user");
			usr.css("display", "none");
		});
	});
</script>
	
<title>Cloud Password</title>
</head>

<body>
	<div id="header" class="shadow">
		<div id="wrapper">		
			<a href="#" class="logo_small">
				<img class="header_logo" src="../images/small.png" width="77px"/>
			</a>
			<?php
				$is_loged = false;
				$username = $_SESSION["user"];
				echo '<a href="#" onclick="showDiv()" class="btn login blue">' . $username . '</a>';
			?>
		</div>
	</div>
	
	<div id="core">
	
		<!--<div id="menu">
			<div id="search">
				<input id="search-input" type="text" name="search">
				<input id="search-button" type="submit">
			</div>
		</div>-->
		
		<div id="user">
			<?php
				echo '<h2>' . $_SESSION["user"] . '</h2>';
				echo '<a href="index.php" class="btn blue userB x2">Passwords</a>';
				echo '<a href="profile.php" class="btn blue userB x2">Profile</a>';
				echo "<p class='border_b'></p>";
				echo '<a href="logout.php" class="btn blue userB" style="margin-top: 5px;">Logout</a>';
			?>
		</div>
		
		<div id="content">
			<?php
				/* displaying passwords from db */
				if(isset($_SESSION["user"])) {
					$data = getAllPasswords($_COOKIE["user"]);
					$rows = $data->num_rows;
					for($i = 0; $i < $rows; $i++) {
						$c = '.pass' . $i;
						$pass = $data->fetch_assoc();
						$file = imageFileName($pass["name"]);
						if($file)
							echo '<a class="btn pass_button inline" href="' . $c . '">
									<img src="' .$file . '">
									<span class="p_name">' . $pass["name"] . '</span>
								 </a>';
						else echo '<a class="btn pass_button inline" href="' . $c . '">'.$pass["name"].'</a>';
					}
					if($rows % 4 != 0)
						echo '<a class="btn pass_button add inline" href="#pop-over" ></a>';
					else 
						echo '<a class="btn pass_button add inline" href="#pop-over" style="clear:both;"></a>';
				}
			?>

			<!-- form for inserting new password -->
			<div style="display: none; width: 300px;">
				<div id="pop-over">
					<form action="add_password.php" method="post">
						<h2>Add password:</h2>
						<p>Name:<br><input type="text" name="app_name" class="textarea"></p>
						<p>Username:<br><input type="text" name="app_uname" class="textarea"></p>
						<p>Password:<br><input type="password" name="app_pass" class="textarea"></p>
						<input type="submit" value="Save" class="btn fButton">
					</form>
				</div>
			</div>
			
			<!-- displaying the password -->
			<?php
				$data = getAllPasswords($_SESSION["user"]);
				$rows = $data->num_rows;
				for($i = 0; $i < $rows; $i++) { 
					$c = 'pass' . $i;
					echo '<div style="display: none">';
						echo '<div id="display_pass" class="' . $c . '">';
							$pass = $data->fetch_assoc();
							echo '<h2>' . $pass["name"] . '</h2>';
							echo '<p>Username: </p><p class="q">' . $pass["username"] . '</p><br>';
							echo '<p>Password: <p class="q">' . $pass["password"] . '</p><br>';
							echo '<h2></h2>';
							echo '<a href="delete.php?user='.$_SESSION["user"].'&name='.$pass["name"].'" class="btn blue long userb">Delete password</a>';
						echo '</div>';
					echo '</div>';
				}
			?>
			
		</div>
	</div>
</body>
</html>
<? ob_end(); ?>