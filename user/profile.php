<?php
	if(!isset($_COOKIE["user"])) {
		header("Location: ../login/");
	}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link rel="stylesheet" type="text/css" href="../css/inputs.css" />
<link rel="stylesheet" type="text/css" href="../css/colorbox.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../js/jquery.colorbox-min.js"></script>

<script>
	$(document).ready(function(){
		$("#header .btn").click(function() {
			var usr = $("#user");
			if(usr.is(':hidden'))
				usr.css("display", "block");
			else
				usr.css("display", "none");
		});
	});
</script>
	
<title>Profile</title>
</head>

<body>

	<div id="header" class="shadow">
		<?php
			echo '<div id="wrapper">';
				$is_loged = false;
				$username = "";
				if(isset($_COOKIE["user"])){
					$is_loged = true;
					$username = $_COOKIE["user"];
				}
				if($is_loged) {
					echo '<a href="#" onclick="showDiv()" class="btn login blue">' . $username . '</a>';
				}
				echo '<a href="login/index.php" class="txt login">Log In</a>';
				echo '<a href="about.php" class="txt login">About</a>';
			echo '</div>';
		?>
	</div>
	
	<div id="core">
	
		<!--<div id="menu">
			<div id="search">
				<input id="search-input" type="text" name="search">
				<input id="search-button" type="submit">
			</div>
		</div>-->
		
		<div id="user">
			<?php
				echo '<h2>' . $_COOKIE["user"] . '</h2>';
				echo '<a href="index.php" class="btn blue userB x2">Passwords</a>';
				echo '<a href="profile.php" class="btn blue userB x2">Profile</a>';
				echo "<h2></h2>";
				echo '<a href="logout.php" class="btn blue userB" style="margin-top: 5px;">Logout</a>';
			?>
		</div>
		
		<div id="content">
			<?php
				if(isset($_COOKIE["user"])) {
					include "../login/database.php";
					$data = getAllPasswords($_COOKIE["user"]);
					$rows = $data->num_rows;
					
					echo "<h2 class='border_b'>Your profile!</h2>";
					echo "<p>Number of password: " . $rows . "</p>";
					echo "<p><b>Your passwords:</b></p>";
					echo "<ul>";
					for($i = 0; $i < $rows; $i++) {
						$c = '.pass' . $i;
						$pass = $data->fetch_assoc();
						echo "<li><p>" . $pass["name"] . ", " . $pass["username"] . "</p></li>";
					}
					echo "</ul>";
					echo "<p class='border_b'></p>";
					echo "<p>Your username: " . $_COOKIE["user"] . "</p>";
					echo "Date: ";
				}
			?>
	</div>
</body>

</html>