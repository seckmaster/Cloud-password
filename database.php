<?php
	$host = "localhost"; // Host name 
	$username = "toni"; // Mysql username 
	$password = ""; // Mysql password 
	$db_name = "c_password"; // Database name 
	
	// connect to database
	$mysqli = new mysqli("$host", "$username", "$password", "$db_name");
	
	// if unable to connect, exit
	if($mysqli->connect_errno) {
		echo "Error accessing database!";
		exit();
	}
	
	// evaluates string if matches requirements
	function Evaluate($string) {
		if(strlen($string) < 8) return false;
		$has_special = false;
		for ($i = 0; $i<strlen($string); $i++)  { 
			$char = substr($string, $i, 1);
			if($char >= '0' && $char <= '9') $has_special = true;
		}
		return $has_special;
	}
	
	// encrypts string (source: http://alias.io/2010/01/store-passwords-safely-with-php-and-mysql/)
	function _Crypt($string) {
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		$cost = 10;
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
		return crypt($string, $salt);
	}
	
	// fn: loging into c. password
	// return true on success
	function LogIn($username, $password) {
		global $mysqli;
		$query = "SELECT * FROM users WHERE username='$username' LIMIT 1";
		if($result = $mysqli->query($query)) {
			if($result = $result->fetch_assoc()) {
				$hash = $result["password"];
				if($password == $hash) {
					$_SESSION["user"] = $username;
					header("Location: ../user/index.php");
					exit();
				}
				return false;
			}
			echo "Wrong username or password!<br>";
			return false;
		}
		return false;
	}
	
	// fn: send email
	function SendMail($to) {
		$subject = "Thank you for registering into Cloud Password!";
		$from = "support@c_password.com";
		$headers = "From:" . $from;
		$headers .= "Reply-To: " . $from. "\r\n";
		$headers .= "CC: " . $from . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$message = '<html><body>';
		$message .= '<h1>Thank you for registering!</h1>';
		$message .= '<p>Hope you enjoy using Cloud Password!</p>';
		$message .= '</body></html>';
		mail($to,$subject,$message,$headers);
	}
	
	// fn: register
	// return true on success
	function Register($username, $password, $email) {
		// if inserted password isn't ok
		/*$p = Evaluate($password);
		if(!$p) {
			echo "Password not strong enough!";
			return;
		}*/
		
		global $mysqli;
		$query = "SELECT * FROM users WHERE username='$username' LIMIT 1";
		if($result = $mysqli->query($query)) {
			if($result->fetch_assoc() == null) {
				$hash = $password;
				$query = "INSERT INTO users VALUES(null, '$username', '$hash', '$email');";
				if($result = $mysqli->query($query)) {
					SendMail($email);
					echo "Registration was successful.<br>Log In!";
				}
				else echo "Error: " . $mysqli->error;
			}
			else echo "User '" .$username . "' already exists!" ;
		}
		else echo "Error: " . $mysqli->error;
	}
	
	// fn: add password into database
	function AddPassword($user_loged, $name, $username, $password) {
		global $mysqli;
		$name = strtolower($name);
		// encrypt password
		$password = _Crypt($password);
		// we need id of loged user
		$query = "SELECT * FROM users WHERE username='$user_loged' LIMIT 1";
		$result = $mysqli->query($query);
		$result = $result->fetch_assoc();
		$id = $result["id"];
		
		// insert data into db
		$query = "INSERT INTO data VALUES(null, '$id', '$name', '$username', '$password');";
		$mysqli->query($query);
		echo $mysqli->error;
	}
	
	// fn: delete password drom db
	function deletePassword($user, $name) {
		global $mysqli;
		// we need id of loged user
		$query = "SELECT * FROM users WHERE username='$user' LIMIT 1";
		$result = $mysqli->query($query);
		$result = $result->fetch_assoc();
		$id_user = $result["id"];
		
		$query = "DELETE FROM data WHERE id_user='$id_user' AND name='$name' LIMIT 1";
		// delete row
		$mysqli->query($query);
		echo $mysqli->error;
	}
	
	// fn: returns all passwords from user
	function getAllPasswords($user) {
		global $mysqli;
		// we need id of loged user
		$query = "SELECT * FROM users WHERE username='$user' LIMIT 1";
		$result = $mysqli->query($query);
		$result = $result->fetch_assoc();
		$id = $result["id"];
		
		// get passwords from data
		$query = "SELECT * FROM data WHERE id_user='$id'";
		$result = $mysqli->query($query);
		return $result;
	}
?>