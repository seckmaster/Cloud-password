<?php
	// Start the session
	if(session_id() == '' || !isset($_SESSION)) {
	    // session isn't started
	    session_start();
	}

	var_dump($_SESSION);

	ob_start();

	if (isset($_SESSION["user"])) {
		header("Location: user/");
		exit();
	}

	include "../database.php";
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/inputs.css" />
<link rel="stylesheet" type="text/css" href="css/colorbox.css" />
<link href="http://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Allerta" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<title>Cloud Password</title>

<script>
	$(function(){
		$("#slideshow > div:gt(0)").hide();

		setInterval(function() { 
		  $('#slideshow > div:first')
			.fadeOut(1000)
			.next()
			.fadeIn(1000)
			.end()
			.appendTo('#slideshow');
		},  3000);
	});
</script>
</head>

<body>
	<div id="header" class="shadow">
		<div class="container">
			<a href="#" class="logo_small">
				<img class="header_logo" src="images/small.png" width="77px"/>
			</a>
			<a href="login/index.php" class="txt">Sign Up</a>
			<a href="about.php" class="txt">About</a>
		</div>
	</div>
	
	<div id="wrap">
		<div class="container">
			<div id="slideshow">
				<div class="anim">
					<h2>Fast</h2>
					<img src="images/clock.png">
				</div>
				<div class="anim">
					<h2>Fun</h2>
					<img src="images/tup.png">
				</div>
				<div class="anim">
					<h2>Free</h2>
					<img src="images/money.png">
				</div>
				<ul>
					<li><a href="#" class="panel"></a></li>
					<li><a href="#" class="panel"></a></li>
					<li><a href="#" class="panel"></a></li>
				</ul>
			</div>
			<div id="logo_container">
				<img src="images/logo2.png" class="logo"/>
				<img src="images/text.png"/>
			</div>
		</div>
		
		<div id="whatIsCP">
			<ul class="container">
				<li>
					<div id="li1">
						<h2>It's fast</h2>
						<p>Have you evere forgotten your password and then it took you ages to recover it? Not anymore! <br>Cloud Password allows you to save all your passwords risk free online. <br>FOR FREE!</p>
						<img src="images/clock.png">
					</div>
				</li>
				<li>					
					<div id="li2">
						<h2>It's fun</h2>
						<p>Add passwords with ease! Using Cloud Password's very simple interface have never been so easy before. Add, remove or edit passwords and never forget them again.</p>
						<img src="images/tup.png">
					</div>
				</li>
				<li>
					<div id="li3">
						<h2>It's free</h2>
						<p>All this awesome features of Cloud Password are absolutely free and will remain free 4ever. <br>Our team will keep working on this project to give you best experience possible.</p>
						<img src="images/money.png">
					</div>
				</li>
			</ul>
		</div>
		<div id="noName">
		
		</div>
	</div>
</body>

</html>