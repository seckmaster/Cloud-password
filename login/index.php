<?php
	ob_start();
	include "../database.php";
	
	/* If user is loged in */
	if (isset($_SESSION["user"])) {
		header("Location: ../user/index.php");
		exit();
	}

	var_dump($_SESSION);

?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../css/login.css" />
<link rel="stylesheet" type="text/css" href="../css/inputs.css" />
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<link href="http://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Allerta" rel="stylesheet" type="text/css">
</head>

<body>	
	
	<div id="header" class="shadow">
		<div class="container">
			<a href="../" class="logo_small">
				<img class="header_logo" src="../images/small.png" width="77px"/>
			</a>
			<a href="#" class="txt">Sign Up</a>
			<a href="../about.php" class="txt">About</a>
		</div>
	</div>
	
	<div id="form" class="animated popIn">
		<div id="center">
			<div id="form_login">
				<form action="index.php" method="post">
					<h2>Log In:</h2>
					<p>Username:&nbsp;<br><input type="text" name="name" class="textarea"></p>
					<p>Password:&nbsp;<br><input type="password" name="pass" class="textarea"></p>
					<input type="submit" value="Log In" class="btn fButton">
				</form>
				<?php
					if(!empty($_POST["name"]) || !empty($_POST["pass"])) {
						Login($_POST["name"], $_POST["pass"]);
					}
				?>
				<p><a href="#">Forgot your password?</a></p>
			</div>
			
			<div id="form_register">
				<form action="index.php" method="post">
					<h2>Create new account:</h2>
					<p>Username:&nbsp;<br><input type="text" name="username" class="textarea"></p>
					<p>E-mail:&nbsp;<br><input type="text" name="mail" class="textarea"></p>
					<p>Password:&nbsp;<br><input type="password" name="password" class="textarea"></p>
					<input type="submit" value="Register" class="btn fButton">
				</form>
				<p>
					<?php
						if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["mail"])) {
							if(Register($_POST["username"], $_POST["password"], $_POST["mail"])) {
								$_POST["name"] = $_POST["username"];
							}
						}
					?>
				</p>
			</div>
		</div>
	</div>
</body>
</html>

<?php 
ob_end();
?>